import main

"""Tests for main.py using Pytest"""

def test_numbers():
    if main.entry + main.entry_two == main.answer_one:
        assert main.answer_one == 2020
        return main.answer_one


def test_adding_multiple_numbers():
    if main.entry + main.entry_two + (main.remaining_amount - main.entry_two) == 2020:
        assert main.entry * main.entry_two * (main.remaining_amount - main.entry_two) == main.answer_two
        return main.answer_two


def test_get_correct_passwords():
    passwords = main.valid_password
    if int(main.v1) <= main.value.count(main.v) <= int(main.v2):
        assert len(passwords) != 0


def test_passwords_auth():
    count = 0    
    if main.value[main.val1-1] == main.val:
        # print(value[val1-1])
        count+=1

    if main.value[main.val2-1] == main.val:
        count+=1

    if count == 1:
        # print(count)
        main.correct_password.append(main.value)
 
    assert main.correct_password != ""