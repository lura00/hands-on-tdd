
with open('aoc_data.txt', 'r') as data:
    entries = set([int(line) for line in data.readlines()])

for entry in entries:
    if 2020 - entry in entries:
        entry_two = 2020 - entry
        answer_one = entry * entry_two
        break

print("Product of the two entries that sum to 2020:", answer_one)



answer_two = False
for entry in entries:
    remaining_amount = 2020 - entry
    for entry_two in entries:
        if remaining_amount - entry_two in entries and entry_two != entry:
            answer_two = entry * entry_two * (remaining_amount - entry_two)
            break

    if answer_two:
        break

# Answer Two
print("Product of the three entries that sum to 2020:", answer_two)


data_dir = "./"
day2 = open(data_dir+"aoc_data_day2.txt").readlines()
day2 = [line.strip() for line in day2]


valid_password = []

for line in day2:
    key, value = line.split(": ")
    # print(line)
    # print(key, value)
    v1, v2 = key.split(" ")[0].split("-")
    v = key.split(" ")[1]

    if int(v1) <= value.count(v) <= int(v2):
        valid_password.append(value)
        # print(key, value, v1, v2, v)


print(len(valid_password))

correct_password = []
for line in day2:
    key, value = line.split(': ')

    vint = lambda x: [int(i) for i in x]

    val1,val2 = key.split(' ')[0].split('-')
    val1, val2 = vint([val1, val2]) 
    val = key.split(' ')[1]
    print(val)

    count = 0    
    if value[val1-1] == val:
        # print(value[val1-1])
        count+=1

    if value[val2-1] == val:
        count+=1

    if count == 1:
        # print(count)
        correct_password.append(value)
print(val)
print(len(correct_password))

